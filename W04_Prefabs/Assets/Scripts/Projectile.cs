﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	// Used to keep track of how long projectile is alive for
	public float lifeTime;

	// Used to move projectile in Scene
	public float speed;

	// Use this for initialization
	void Start () {

		// Check if lifeTime variable has been set
		if(lifeTime <=0 )
		{
			// Variable was not set. Print warning to User
			Debug.LogWarning("lifeTime not set. Defaulting to 1 sec.");

			// Apply default value of 1 second.
			lifeTime = 1.0f;
		}
	
		// Delete GameObject script is attached to after lifeTime seconds
		Destroy (gameObject, lifeTime);

		//GetComponent<Rigidbody2D> ().velocity = new Vector2(speed, 0);
		GetComponent<Rigidbody2D> ().velocity = Vector2.right * speed;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
