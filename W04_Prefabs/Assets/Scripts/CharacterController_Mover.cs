﻿using UnityEngine;
using System.Collections;

public class CharacterController_Mover : MonoBehaviour {
   
    // Method 1: Reference Rigidbody through script
    Rigidbody2D rb;

    // Method 2: Reference Rigidbody through Inspector
    public Rigidbody2D rb2; 

    // Handle movement speed of Character
    // - Can be adjusted through Inspector while in Play mode
    public float speed;

    // Handles jump speed of Character
    public float jumpForce;         // How high the character will Jump
    public bool isGrounded;         // Is the player touching the groun
    public LayerMask isGroundLayer; // What is the Ground? Player can only jump on things that are on the "Ground" layer
    public Transform groundCheck;   // Used to figure out if the player is touching the ground

    // Handles animations
    Animator anim;

	// Handles flipping Character
	public bool isFacingLeft;

	// Use this for initialization
	void Start () {

        // Method 1: Reference Rigidbody through script
		rb = GetComponent<Rigidbody2D> ();
        
        // Checks if Rigidbody2D exists
        // - Both methods need this
        if(!rb)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No Rigidbody2D found on Character.");
        }

        if (!rb2)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No Rigidbody2D found on Character.");
        }
       
        // Check if speed was set to something not 0
        if(speed == 0)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogWarning("Speed not set in Inspector. Defaulting to 5.");
            
            // Assign a default value if one is not set in the Inspector
            speed = 5.0f;
        }

        // Check if jumpForce was set to something not 0 
        if (jumpForce == 0)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogWarning("JumpForce not set in Inspector. Defaulting to 5.");
            
            // Assign a default value if one is not set in the Inspector
            jumpForce = 5.0f;
        }

        // Reference Animator through script
        // - Can still use Method 2 if you want
        anim = GetComponent<Animator>();

        // Checks if Animator exists
        if(!anim)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.LogError("No Animator found on Character.");
        }
	}
	
	// Update is called once per frame
	void Update () {

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, isGroundLayer);

        // Checks if Left (or a) or Right (or d) is pressed
        // - "Horizontal" must exist in Input Manager (Edit-->Project Settings-->Input)
        // - Returns -1(left), 1(right), 0(nothing)
        // - Used GetAxis for value -1-->0-->1 and all decimal places. (Gradual change in values)
        float moveValue = Input.GetAxisRaw("Horizontal");

        //Debug.Log(moveValue);

        // Check if "Jump" button was pressed
        // - "Jump" must exist in Input Manager (Edit-->Project Settings-->Input)
        // - Configuration can be changed later
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Jump.");

            // new Vector2(0,1);    --> Vector2.up
            // new Vector2(0,-1);   --> Vector2.down
            // new Vector2(1,0);    --> Vector2.right
            // new Vector2(-1,0);   --> Vector2.left

            // Applies a force in the UP direction
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

        // Check if Left Control was pressed\
        // - Tied to key and cannot be changed
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            // Prints a message to Console (Shortcut: Control+Shift+C)
            Debug.Log("Fire.");

			// Set transition to true to play shoot animation
            anim.SetBool("Shoot", true);
        }
        else
			anim.SetBool("Shoot", false);// Set transition to false to stop playing shoot animation
	
		// Check if Left and Down are pressed
		if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.DownArrow)) 
		{
			// Apply a force to the Left
			rb.AddForce (Vector2.left * 800, ForceMode2D.Impulse);

			// Set transition to true to play slide animation
			anim.SetBool("Slide", true);
		}
		else
			anim.SetBool("Slide", false);	// Set transition to false to stop playing slide animation


        // Move Character using Rigidbody2D
        // - Uses moveValue from GetAxis to move left or right
        rb.velocity = new Vector2(moveValue * speed, rb.velocity.y);

        // Tells Animator to transition to another Clip
        // - Parameter must be created in Animator window
        anim.SetFloat("Speed", Mathf.Abs(moveValue));

		// Check if Character should flip to look left or right
		if ((isFacingLeft && moveValue > 0)||(!isFacingLeft && moveValue < 0))
		{
			// Call function to flip character
			flipCharacter ();
		}
	}

	void flipCharacter()
	{
		// Method 1: Toggle isFacingLeft variable
		isFacingLeft = !isFacingLeft;

		// Method 2: Toggle isFacingLeft variable - Choose one
		/*if (isFacingLeft)
			isFacingLeft = false;
		else 
			isFacingLeft = true;
		*/

		// Make a copy of old scale value
		Vector3 scaleFactor = transform.localScale;

		// Flip scale of x
		scaleFactor.x *= -1; // scaleFactor.x = -scaleFactor.x;

		// Update scale to new flipped value
		transform.localScale = scaleFactor;
	}
}







