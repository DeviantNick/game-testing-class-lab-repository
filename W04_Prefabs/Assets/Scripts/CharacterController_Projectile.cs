﻿using UnityEngine;
using System.Collections;

public class CharacterController_Projectile : MonoBehaviour {

	// Where to create prefab in Scene
	public Transform projectileSpawnPoint;

	// Prefab to create (instantiate)
	public Projectile projectile;

	// Use this for initialization
	void Start () {

		// Always check that everything has been initialized
		if (!projectileSpawnPoint)
			Debug.LogError ("Projectile spawnpoint missing.");

		if (!projectile)
			Debug.LogError ("Projectile prefab missing.");
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetButtonDown("Fire1"))
		{
			// Create Projectile and add to Scene
			// - Need prefab
			// - Location to spawn
			// - Rotation of spawn
			Projectile temp = Instantiate(projectile, 
			                              projectileSpawnPoint.position, 
			                              projectileSpawnPoint.rotation)
				as Projectile;

			// Apply movement speed
			//temp.GetComponent<Projectile>().speed = 5.0f;
			//temp.GetComponent<Rigidbody2D>().velocity = new Vector2(5.0f, 0);
			//temp.GetComponent<Rigidbody2D>().velocity = Vector2.right * 5.0f;
			temp.speed = 7.0f;

		}
	}
}








